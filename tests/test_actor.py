"""Test the classes and functions defined by gg/actor.py"""

from unittest.mock import Mock, call

from tests import BaseTestCase


class ActorTestCase(BaseTestCase):
    filename = 'actor'

    def setUp(self):
        super().setUp()
        self.mod.Champlain.Point.set_color = Mock()
        self.mod.Champlain.Point.set_size = Mock()
        self.mod.Clutter.Color.new = Mock()
        self.mod.Gst = Mock()
        self.mod.Gtk.RadioMenuItem.__init__ = Mock()
        self.mod.Gtk.RadioMenuItem.connect = Mock()
        self.mod.Gtk.RadioMenuItem.set_label = Mock()
        self.mod.MapView.bin_layout_add = Mock()
        self.mod.Scale.connect_view = Mock()
        self.mod.Widgets = Mock()

    def test_radiomenuitem(self):
        """Ensure we can create a RadioMenuItem."""
        source = Mock()
        self.mod.RadioMenuItem.cache.clear()
        rmi = self.mod.RadioMenuItem(source)
        self.mod.Gtk.RadioMenuItem.__init__.assert_called_once_with()
        rmi.set_label.assert_called_once_with(source.get_name.return_value)
        rmi.connect.assert_called_once_with(
            'activate', rmi.menu_item_clicked, source.get_id.return_value)
        self.mod.Widgets.map_source_menu.append.assert_called_once_with(rmi)
        self.mod.RadioMenuItem(Mock())
        rmi.set_property.assert_called_once_with('group', rmi)

    def test_radiomenuitem_menu_item_clicked(self):
        """Ensure we set the map when a map source is clicked."""
        map_id = Mock()
        source = Mock()
        self.mod.MAP_SOURCES = {map_id: source}
        rmi = self.mod.RadioMenuItem(Mock())
        rmi.menu_item_clicked(None, map_id)
        self.mod.MapView.set_map_source.assert_called_once_with(source)

    def test_sources(self):
        """Ensure we can initialize the map source menu."""
        self.mod.Gst.get_string.return_value = 'osm-mapnik'
        self.mod.RadioMenuItem = Mock()
        self.mod.Sources.__init__()
        self.mod.Gst.get_string.assert_called_once_with('map-source-id')
        expected = [call(v) for k, v in sorted(self.mod.MAP_SOURCES.items())]
        expected = expected[:4] + [call().set_active(True)] + expected[4:]
        self.assertEqual(len(expected), len(self.mod.RadioMenuItem.mock_calls))
        for i, exp in enumerate(expected):
            self.assertEqual(self.mod.RadioMenuItem.mock_calls[i], exp)

    def test_crosshair(self):
        """Ensure the crosshair is created correctly."""
        self.mod.Crosshair.__init__()
        self.mod.Champlain.Point.set_size.assert_called_once_with(4)
        self.mod.Champlain.Point.set_color.assert_called_once_with(
            self.mod.Clutter.Color.new.return_value)
        self.mod.Clutter.Color.new.assert_called_once_with(0, 0, 0, 64)
        self.mod.Gst.bind.assert_called_once_with(
            'show-map-center', self.mod.Crosshair, 'visible')
        self.mod.MapView.bin_layout_add.assert_called_once_with(
            self.mod.Crosshair,
            self.mod.Clutter.BinAlignment.CENTER,
            self.mod.Clutter.BinAlignment.CENTER)

    def test_scale(self):
        """Ensure the scale is created correctly."""
        self.mod.Scale.__init__()
        self.mod.Scale.connect_view.assert_called_once_with(self.mod.MapView)
        self.mod.Gst.bind.assert_called_once_with(
            'show-map-scale', self.mod.Scale, 'visible')
        self.mod.MapView.bin_layout_add.assert_called_once_with(
            self.mod.Scale,
            self.mod.Clutter.BinAlignment.START,
            self.mod.Clutter.BinAlignment.END)

    def test_coordlabel(self):
        """Ensure the coordinate label is created correctly."""
        self.mod.CoordLabel.__init__()
        self.mod.CoordLabel.set_color.assert_called_once_with(
            self.mod.Clutter.Color.new.return_value)
        self.mod.Clutter.Color.new.assert_called_once_with(255, 255, 255, 255)

    def test_box(self):
        """Ensure the coord box is created correctly."""
        self.mod.Box.set_layout_manager = Mock()
        self.mod.Box.__init__()
        self.mod.Box.set_layout_manager.assert_called_once_with(
            self.mod.Clutter.BinLayout.return_value)
        self.mod.CoordLabel.set_color.assert_called_once_with(
            self.mod.Clutter.Color.new.return_value)
        self.mod.Clutter.Color.new.assert_called_once_with(0, 0, 0, 96)
        self.mod.Box.connect_view.assert_called_once_with(self.mod.MapView)
        self.mod.Gst.bind.assert_called_once_with(
            'show-map-coords', self.mod.Box, 'visible')
        self.mod.MapView.bin_layout_add.assert_called_once_with(
            self.mod.Box,
            self.mod.Clutter.BinAlignment.START,
            self.mod.Clutter.BinAlignment.START)

    def test_animate_in(self):
        """Animate in all widgets."""
        self.mod.sleep = Mock()
        self.mod.Gst.get_int.return_value = 5
        self.mod.Box.set_opacity = Mock()
        self.mod.animate_in(True)
        self.mod.Gst.get_int.assert_called_once_with('animation-steps')
        self.assertEqual(self.mod.Box.set_opacity.call_count, 5)
        self.assertEqual(self.mod.Widgets.redraw_interface.call_count, 5)
